---
layout: job_page
title: "Expert"
---

Expert means you have above average experience with a certain topic.
Commonly you're expert in multiple topics after working at GitLab for some time.
This helps people in the company to quickly find someone who knows more.
Please add these labels to yourself and assign the merge request to your manager.

For Production Engineers, a listing as "Expert" can also mean that the individual
is actively [embedded with](/handbook/infrastructure/#embedded) another team.
Following the period of being embedded, they are experts in the regular sense
of the word described above.


Developers focused on Reliability and Production Readiness are named [Reliability Expert](/jobs/expert/reliability/).
